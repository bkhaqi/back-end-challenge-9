const db = require('../models')
const Game = db.games

const gamesSeedData = [
    {
        title: "Rock Paper Scissor",
        description: "The familiar game of Rock, Paper, Scissors is played like this: at the same time, two players display one of three symbols: a rock, paper, or scissors. A rock beats scissors, scissors beat paper by cutting it, and paper beats rock by covering it.",
        thumbnail: "https://i.ibb.co/8cj8wHx/top-title.webp",
        play_count: 0,
        how_to_play: "Rock paper scissor",
        release_date: new Date(),
        latest_update: "v1.0"
    },
    {
        title: "Tic-Tac-Toe",
        description: "A game in which two players seek in alternate turns to complete a row, a column, or a diagonal with either three O's or three X's drawn in the spaces of a grid of nine squares.",
        thumbnail: "https://i.ibb.co/gdskP42/tic-tac-toe.png",
        play_count: 0,
        how_to_play: "Tic-Tac-Toe",
        release_date: new Date(),
        latest_update: "v1.0"
    }
]

async function seedGames() {
    try {
      await Game.bulkCreate(gamesSeedData);
  
      console.log("Games seeded successfully.");
    } catch (error) {
      console.error("Error seeding games:", error);
    }
  }
  
  module.exports = seedGames;