const seedGames = require('./game.seed')
const seedScore = require('./score.seed')

seedGames();
seedScore();
