const bodyParser = require('body-parser')
const gameControllerAPI = require('../../controllers/api/game.api.controller')

const jsonParser = bodyParser.json()
const router = require("express").Router()

router.get('/id/:id', gameControllerAPI.getGame)
router.get('/all', gameControllerAPI.getAllGames)

module.exports = router;