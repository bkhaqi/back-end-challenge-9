const bodyParser = require('body-parser')
const scoreControllerAPI = require('../../controllers/api/score.api.controller')

const jsonParser = bodyParser.json()
const router = require("express").Router()

router.post('/add/:id/:gameId', scoreControllerAPI.addScore)
router.get('/get/:id', scoreControllerAPI.getScore)
router.get('/all', scoreControllerAPI.getAllScore)
router.get('/leaderboard/:gameId', scoreControllerAPI.Leaderboard)

module.exports = router;